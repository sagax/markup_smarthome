express = require 'express'
router = express.Router()
db_access = require './db_access'


# helper functions
get_random = (min, max) ->
  random = Math.floor(Math.random() * (max - min + 1) + min)
  return random

responsing = (res, _object) ->
  res.json _object
  return

###############################################################################

# get all items
router.get '/items', (req, res, next) ->
  client = db_access.get_client()
  client.scan 0, 'match', 'item:*', 'count', 1000, (err, data) ->
    client.mget data[1], (err, _data) ->
      items = []
      if _data isnt undefined
        for el in _data
          items.push JSON.parse(el)

      res.json items
      client.quit()
      return
    return
  return

# get item
router.get '/item/:id', (req, res, next) ->
  if req.params.id then id = req.params.id else id = null

  client = db_access.get_client()
  client.get ['item', ':', id].join(''), (err, data) ->
    res.json JSON.parse(data)
    client.quit()
    return
  return

# save item
router.post '/item/:id', (req, res, next) ->
  id = req.params.id

  item_to_save = JSON.stringify( JSON.parse req.body['data'] )

  client = db_access.get_client()
  client.set ['item', ':', id].join(''), item_to_save
  client.quit()

  res.status 200
  res.send save: "ok"
  return

# update item
router.put '/item/:id', (req, res, next) ->
  id = req.params.id

  item_to_save = JSON.stringify( JSON.parse req.body['data'] )

  client = db_access.get_client()
  client.set ['item', ':', id].join(''), item_to_save
  client.quit()

  res.status 200
  res.send update: "ok"
  return

# delete item
router.delete '/item/:id', (req, res, next) ->
  id = req.params.id

  client = db_access.get_client()
  client.get ['item', ':', id].join(''), (err, data) ->
    if data isnt null or data isnt undefined
      client.del ['item', ':', id].join('')
      client.quit()
    return

  res.status 200
  res.send delete: "ok"
  return

###############################################################################

# get all tools
router.get '/tools', (req, res, next) ->
  client = db_access.get_client()
  client.scan 0, 'match', 'tool:*', 'count', 1000, (err, data) ->
    client.mget data[1], (err, _data) ->
      tools = []
      if _data isnt undefined
        for el in _data
          tools.push JSON.parse(el)

      res.json tools
      client.quit()
      return
    return
  return

# get tool
router.get '/tool/:id', (req, res, next) ->
  if req.params.id then id = req.params.id else id = null

  client = db_access.get_client()
  client.get ['tool', ':', id].join(''), (err, data) ->
    res.json JSON.parse(data)
    client.quit()
    return
  return

# save tool
router.post '/tool/:id', (req, res, next) ->
  id = req.params.id

  tool_to_save = JSON.stringify( JSON.parse req.body['data'] )

  client = db_access.get_client()
  client.set ['tool', ':', id].join(''), tool_to_save
  client.quit()

  res.status 200
  res.send save: "ok"
  return

# update tool
router.put '/tool/:id', (req, res, next) ->
  id = req.params.id

  tool_to_save = JSON.stringify( JSON.parse req.body['data'] )

  client = db_access.get_client()
  client.set ['tool', ':', id].join(''), tool_to_save
  client.quit()

  res.status 200
  res.send update: "ok"
  return

# delete tool
router.delete '/tool/:id', (req, res, next) ->
  id = req.params.id

  client = db_access.get_client()
  client.get ['tool', ':', id].join(''), (err, data) ->
    if data isnt null or data isnt undefined
      client.del ['tool', ':', id].join('')
      client.quit()
    return

  res.status 200
  res.send delete: "ok"
  return


###############################################################################

# get all sensors
router.get '/sensors', (req, res, next) ->
  client = db_access.get_client()
  client.scan 0, 'match', 'sensor:*', 'count', 1000, (err, data) ->
    client.mget data[1], (err, _data) ->
      sensors = []
      if _data isnt undefined
        for el in _data
          sensors.push JSON.parse(el)

      res.json sensors
      client.quit()
      return
    return
  return

# get sensor
router.get '/sensor/:id', (req, res, next) ->
  if req.params.id then id = req.params.id else id = null

  client = db_access.get_client()
  client.get ['sensor', ':', id].join(''), (err, data) ->
    res.json JSON.parse(data)
    client.quit()
    return
  return

# get sensor update data
router.get '/sensor/:id/data', (req, res, next) ->
  res.json get_random(0, 30)
  return

# save sensor
router.post '/sensor/:id', (req, res, next) ->
  id = req.params.id

  sensor_to_save = JSON.stringify( JSON.parse req.body['data'] )

  client = db_access.get_client()
  client.set ['sensor', ':', id].join(''), sensor_to_save
  client.quit()

  res.status 200
  res.send save: "ok"
  return

# update sensor
router.put '/sensor/:id', (req, res, next) ->
  id = req.params.id

  sensor_to_save = JSON.stringify( JSON.parse req.body['data'] )

  client = db_access.get_client()
  client.set ['sensor', ':', id].join(''), sensor_to_save
  client.quit()

  res.status 200
  res.send update: "ok"
  return

# delete sensor
router.delete '/sensor/:id', (req, res, next) ->
  id = req.params.id

  client = db_access.get_client()
  client.get ['sensor', ':', id].join(''), (err, data) ->
    if data isnt null or data isnt undefined
      client.del ['sensor', ':', id].join('')
      client.quit()
    return

  res.status 200
  res.send delete: "ok"
  return

###############################################################################

# gel all events
router.get '/events', (req, res, next) ->
  client = db_access.get_client()
  client.scan 0, 'match', 'event:*', 'count', 1000, (err, data) ->
    client.mget data[1], (err, _data) ->
      events = []
      if _data isnt undefined
        for el in _data
          events.push JSON.parse(el)

      res.json events
      client.quit()
      return
    return
  return

# get event
router.get '/event/:id', (req, res, next) ->
  if req.params.id then id = req.params.id else id = null

  client = db_access.get_client()
  client.get ['event', ':', id].join(''), (err, data) ->
    res.json JSON.parse(data)
    client.quit()
    return
  return

# save event
router.post '/event/:id', (req, res, next) ->
  id = req.params.id

  event_to_save = JSON.stringify( JSON.parse req.body['data'] )

  client = db_access.get_client()
  client.set ['event', ':', id].join(''), event_to_save
  client.quit()

  res.status 200
  res.send save: "ok"
  return

# update event
router.put '/event/:id', (req, res, next) ->
  id = req.params.id

  event_to_save = JSON.stringify( JSON.parse req.body['data'] )

  client = db_access.get_client()
  client.set ['event', ':', id].join(''), event_to_save
  client.quit()

  res.status 200
  res.send update: "ok"
  return

# delete event
router.delete '/event/:id', (req, res, next) ->
  id = req.params.id

  client = db_access.get_client()
  client.get ['event', ':', id].join(''), (err, data) ->
    if data isnt null or data isnt undefined
      client.del ['event', ':', id].join('')
      client.quit()
    return

  res.status 200
  res.send delete: "ok"
  return

###############################################################################

# get all total
router.get '/get_all', (req, res, next) ->
  _object =
    "items": items
    "tools": tools
    "sensors": sensors
    "events": events
  res.json _object
  return

###############################################################################

# test path
router.get '/test', (req, res, next) ->
  res.redirect '/test.html'
  return

# wrong way
router.get '*', (req, res, next) ->
  res.status 200
  res.send wrong_way: "try again :D"
  return

module.exports = router
