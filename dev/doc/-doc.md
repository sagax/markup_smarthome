# SmartHome Control Panel API DOC

-----

## Оглавление

- Классы
  - Item
  - Tool
  - Sensor
  - Event
  - Coherence
- Вспомогательные функции
- Вспомогательные объекты
- Общее описание некоторых принципов работы приложения

-----

## Класы

## Item

Класс объекта. Объект это комната, помещение, место которое объединяет в себя комлекс устройств по смыслу или по назначению.

**Пример**:
> **Объект** => **Кухня**, и она включается в себя устройства и сенсоры

**Свойства класса**:
- **id**: id объекта
- **name**: имя объекта
- **desc**: описание объекта
- **sensors**: список **id** сенсоров
- **tools**: список **id** устройств
- **psize**: параметры x, y, width, height на панели **_gridster_**
- **list_params**: список валидных параметров

**Методы класса**:

> ЗАМЕТКА: **get** - это описание того, что может получить функция в качестве аргумента. **return** - это описание того что может вернуть функция по завершении работы. **return** описание имеет мало смысла если функция работает асинхронно. В основном функции будут возвращать _true_ и это ничего не значит, просто _true_.

**@init**
- _get_: params - объект со свойствами. Валидные свойства объекта переносятся в self
- _return_: true - возвращает true

**@set_id**
- _get_: ничего
- _return_: true - возможно true

**@to_save_params**
- _get_: ничего
- _return_: params - Возвращает параметры в виде для сохранения

**@save**
- _get_: ничего
- _return_: true - возвращает true

**@update**
- _get_: ничего
- _return_: true - возвращает true

**@delete**
- _get_: ничего
- _return_: true - возвращает true

**@add_tool**
- _get_: params - объекта со свойствами устройства
- _return_: true - возвращает true

**@add_sensor**
- _get_: params - объект со свойствами сенсора.
- _return_: true - возвращает true

**@render**
- _get_: ничего
- _return_: true - возсращает true

-----

## Tool

Класс устройства. Устройство это в основном включатели/выключатели и регуляторы по шкале. Возможно объединение устройств в один кластер.

**Пример**:
> **Устройство** => **Включатель** и он включает и выключает свет на кухне.

**Свойства класса**:
- **id**: id устройства
- **name**: имя объекта
- **desc**: описание объекта
- **item_id**: id объекта если устройство привязано к нему
- **type**: тип устройства
- **onoff**: состояние устройства - опционально
- **val**: значение устройства - опционально
- **min**: минимальное значение **val** устройства - опционально
- **max**: максимальное значение **val** устройства - опционально
- **step**: шаг изменения значения устройства - опционально
- **coherence**: **id** класса **coherence** в случае если устройство объединено в кластер - опционально

Опциональные параметры устройства это такие параметры которые в конкретном случае могут использоваться, а могут и не использоваться.

**Методы класса**:

> ЗАМЕТКА: **get** - это описание того, что может получить функция в качестве аргумента. **return** - это описание того что может вернуть функция по завершении работы. **return** описание имеет мало смысла если функция работает асинхронно. В основном функции будут возвращать _true_ и это ничего не значит, просто _true_.

**@init**
- _get_: params - объект со свойствами. Валидные свойства объекта переносятся в self
- _return_: true - возвращает true

**@set_id**
- _get_: ничего
- _return_: true - возможно true

**@to_save_params**
- _get_: ничего
- _return_: params - Возвращает параметры в виде для сохранения

**@save**
- _get_: ничего
- _return_: true - возвращает true

**@update**
- _get_: ничего
- _return_: true - возвращает true

**@delete**
- _get_: ничего
- _return_: true - возвращает true

**@add_tool**
- _get_: params - объекта со свойствами устройства
- _return_: true - возвращает true

**@add_sensor**
- _get_: params - объект со свойствами сенсора.
- _return_: true - возвращает true

**@render**
- _get_: ничего
- _return_: true - возсращает true

-----

## Sensor

-----

## Event

-----

## Coherence

-----

## Вспомогательные функции

> ЗАМЕТКА: **get** - это описание того, что может получить функция в качестве аргумента. **return** - это описание того что может вернуть функция по завершении работы. **return** описание имеет мало смысла если функция работает асинхронно. В основном функции будут возвращать _true_ и это ничего не значит, просто _true_.

**save_ls** - сохранение объекта JS в хранилище LocalStorage
- _get_: name, object, cb
  - name: имя под которым будет сохранен объект;
  - object: тело объекта для сохранения;
  - cb: коллбэк функция;
- _return_: true - возвращает true

**get_ls** - получение объекта JS из хранилища LocalStorage
- _get_: name, cb
  - name: имя под которым будет поиск объекта;
  - cb: коллбэк функция;
- _return_: true - возвращает true

**remove_class_with_pause** - удаление класса css с задержкой
- _get_: el, classname, period, cb
  - el: елемент dom
  - classname: имя удаляемого класса
  - период времени через который надо удалить класс, если не указан то он равен 1 секунду
  - cb: коллбэк функция
- _return_: true - возвращает true

**gen_time_hash** - генерирование хэша вида XXXXXX на основе времени где X это цифры
- _get_: ничего
- _return_: строка вида XXXXXX где X это цифры от 0 до 9

**gen_input** - генерирование dom input элемента
- _get_: options
  - options: объект с параметрами input элемента, на данный момент это [_type_, _value_]

-----

## Вспомогательные объекты

**LeftMenu** - нужен для управления левым меню и взаимодействия этого меню с остальными частями страницы.

> описание этого объекта потом сделаю

**routes** - важный объект для получения правильных **url** во время взаимодействия с http сервером. Настройки берутся в файле **/json/settings.json**. Работа объекта будет показана на примерах ниже.

**routes.init** - метод инициализации настроек для дальнейшей работы routes. Инициализация происходит один раз после отрисовки страниц.
- _get_: data, cb
  - data: это JS объект с настройками из **/json/settings.json[routes]**
  - cb: коллбэк функция
- _return_: true - возвращает true

**routes.path** - метод получения правильного **url**. Например есть **path** c именем: **item_get**, типом: **get**, и uri: **/item/:id**. Еще есть объект **item** со свойством **id** => **item.id** = 100 например. Тогда запустив функцию **routes.path** с аргументами **(path, item)** получим строку вида **/item/100**. Функция **routes.path** ищет свойства объекта описанные в шаблоне и заменяет шаблонные параметры на настоящие. Это означает что для построения своих **uri** запросов к http серверу можно применять собственные шаблоны, например **/item/:id** можно переделать в **/item/get/:id** и т.п.
- _get_: path, object
  - path: строка с именем url, имена url брать из settings.json
  - object: объект для которого делается url
- _return_: строка uri

**APP_SMART_HOME** = **ASH** = **app_smart_home** = **ash** = **app** - важный вспомогательный объект работы приложения в общем и целом. Содержит в себе большую часть функций инициализации и настроек приложения. В документации в дальнейшем этот вспомогательный объект будет именоваться как **app**

**app.init** - функция первоначальной инициализации некоторых параметров.
- _get_:
- _return_:

**app.init_ajax** - функция считывания настроек из **/json/settings.json**. Внутри функции делается асинхронный ajax запрос к файлу **/json/settings.json**. После получения объекта считываются свойства объекта JS [templates, routes]. **routes** отправляется в **routes.init**, а **templates** отправляется в **app.push_setting**
- _get_: ничего
- _return_: true - возвращают true

**app.push_setting** - функция для получения настроек из **settings.json[templates]**. Указанные шаблоны в **settings.json[templates]** скачиваются и сохраняются. Функция делает **ajax** синхронный запрос к html шаблонам указанным в настройках.

В результате работы функции в **window** области видимости появляются JS объекты [items, tools, sensors, events]
- _get_: object, name, cb
  - object: JS объект настроек из **settings.json[templates][]** - в этом объекте указаны все шаблоны представления основных элементов на странице [items, tools, sensors, events]. В этом JS объекте функцию интересуют свойства [name, label, template].
  - name: Имя объекта для представления в **window** области видимости.
  - cb: коллбэк функция
- _return_: ничего

**app.init_env** - функция для получения и добавления объектов [items, tools, sensors, events, ..]
- _get_:
- _return_:

**app.global_event** - функция для создания **event** связей для работы интерфейса.

**app.erase** - переписывает все свойства JS объекта на **null**
- _get_: object - объект JS со свойствами
- _return_: true - возвращает true

**Gridster** - объект JS содержит в себе настройки и параметры gridster jquery плагина.

**Gridster.init** - функция инициализации настроек плагина gridster.

-----

## Общее описание некоторых принципов работы приложения

- **ajax** запросы в основном асинхронные и не кэшированные
- **функции** в основном возвращают просто **true**
