class Event
  constructor: (options) ->
    options = options or {}
    @id = options.id ? null
    @name = options.name ? null
    @desc = options.desc ? null
    @condition_ids = options.condition_ids ? []
    @reaction_ids = options.reaction_ids ? []
    @psize = options.psize ? sizex: 2, sizey: 1
    @list_params = ['id', 'name', 'desc', 'condition_ids', 'reaction_ids', 'psize']

  init: (object) ->
    self = @
    object = object or {}

    for key, value of object when @list_params.indexOf(key) >= 0
      self[key] = value

    if @id is '' or @id is undefined or @id is null
      @set_id()

    if events.collection[@id] is undefined
      events.collection[@id] = @
    true

  set_id: ->
    self = @
    if @id is '' or @id is undefined or @id is null
      @id = gen_time_hash()
      @save()
    true

  to_save_params: ->
    self = @
    params = {}

    for key, value of @ when @list_params.indexOf(key) >= 0
      params[key] = value
    params

  add_condition: (list) ->
    return

  add_reaction: (list) ->
    return

  save: ->
    self = @
    events.collection[@id] = @
    params = JSON.stringify @to_save_params()

    $.ajax
      url: routes.path("event_save", @)
      type: routes["event_save"].type
      data: data: params
      success: (data) ->
        return
      error: (data, status, error) ->
        return
    true

  update: ->
    self = @
    events.collection[@id] = @
    params = JSON.stringify @to_save_params()

    $.ajax
      url: routes.path("event_update", @)
      type: routes["event_update"].type
      data: data: params
      success: (data) ->
        return
      error: (data, status, error) ->
        return
    true

  delete: ->
    self = @
    $.ajax
      url: routes.path("event_delete", @)
      type: routes["event_delete"].type
      success: (data) ->
        delete events.collection[@id]
        return

      error: (data, status, error) ->
        return
    return

  render: ->
    self = @
    raw = $(events.template.main.html)[0]
    $(raw).attr 'data-id', @id
    $(raw).find('[data-id]').attr 'data-id', @id

    $(raw).find('[data-name]')
      .attr 'data-name', @name
      .attr 'data-id', @id
      .text @name

    $(raw).find('[data-desc]')
      .attr 'data-desc', @desc
      .attr 'data-id', @id
      .text @desc

    app.gridster.events.add_widget raw, @psize.sizex, @psize.sizey
    return
