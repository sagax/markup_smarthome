save_ls = (name, object, cb) ->
  console.log "SAVE THIS:", name, object
  try
    ls[name] = JSON.stringify(object)
    if cb
      cb()
  catch error
    console.log "error:", error
  true

get_ls = (name, cb) ->
  console.log "GET THIS:", name
  if ls[name] isnt undefined
    data = JSON.parse ls[name]
    data
  else
    undefined

remove_class_with_pause = (el, classname, period, cb) ->
  period = period or 1000
  setTimeout (->
    $(el).removeClass(classname)
    if cb
      cb()
    return
  ), period
  true
  return

gen_time_hash = ->
  hash = (new Date().getTime() + '').split('').reverse().slice(0, 6).reverse().join('')
  hash

gen_input = (options) ->
  input = document.createElement 'input'
  options = options or {}
  input.type = options.type or 'text'
  input.value = options.value or ''

  if options.attrs
    for key, value of options.attrs
      input.setAttribute(key, value)
  input

