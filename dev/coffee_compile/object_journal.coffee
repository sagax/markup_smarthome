Journal = window.Journal =
  # level, part, message,
  dom: null
  level: []
  init: (el, level) ->
    Journal.dom = el or $('[data-container="journal"]')[0]
    Journal.level = level or []
    return

  write: (level, message) ->
    if Journal.level.indexOf(level) >= 0
      p = document.createElement 'p'
      p.className = 'journal__message'
      p.textContent = message
      $(Journal.dom).append p
    return

  modal: (level, message) ->
    return

  render: ->
    return

  save: ->
    return

