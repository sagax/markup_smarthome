LeftMenu =
  base_el: null
  base_ul: null
  base_content: '[data-content="all"]'
  base_squash_button: null
  base_left_part: null
  base_right_part: null

  init: (name) ->
    Journal.write 9, 'LeftMenu init run'
    if name
      @base_el = $(name)[0]
      @base_ul = $(name + ' ' + 'ul')[0]
      @base_squash_button = $(name + ' ' + '[data-role="squash"]')[0]
      @base_left_part = $('[data-area="left_part"]')[0]
      @base_right_part = $('[data-area="right_part"]')[0]
      @squash(true)
      @listen()
      Journal.write 9, 'LeftMenu init stop -> true'
      true
    else
      Journal.write 'LeftMenu init stop -> false'
      false

  listen: ->
    Journal.write 9, 'LeftMenu listen run'
    self = @
    $(@base_el).on 'click', (event) ->
      event = event or window.event
      target = event.target or event.srcElement

      if target.tagName.toLowerCase() isnt 'a' and $(target).parent()[0].tagName.toLowerCase() is 'a'
        target = $(target).parent()[0]

      render = $(target).attr 'data-render'
      area = $(target).attr 'data-area'
      action = $(target).attr 'data-action'

      if render
        self.render render, target

      if area
        self.area area, target

      if action
        self.action action, target
      return
    Journal.write 9, 'LeftMenu listen stop'
    return

  render: (command, target) ->
    Journal.write 9, 'LeftMenu render run'
    self = @

    if $('[data-area="' + command + '"]').is(':visible') is true
      return true

    else
      $(@base_content + ' ' + '[data-area]').hide()
      $(@base_ul).find('li').removeClass 'pure-menu-active'
      $(@base_content + ' ' + '[data-area="' + command + '"]').fadeIn()
      $(target).parent().addClass 'pure-menu-active'

      if command is 'configure'
        if app['gridster']['items'] is undefined
          Gridster.init.configure.items()
          Gridster.init.configure.tools()
          Gridster.init.configure.sensors()

      else if command is 'logics'
        if app['gridster']['events'] is undefined
          Gridster.init.configure.events()
    Journal.write 9, 'LeftMenu render stop'
    return

  area: (command) ->
    self = @
    return

  action: (command, target) ->
    self = @
    if /squash/.test(command) is true
      @squash()
    return

  squash: (status) ->
    self = @
    if ls.squash is undefined
      save_ls "squash", false

    squash = get_ls "squash"

    if status is true
      if squash is true
        $(self.base_squash_button).removeClass('fa-angle-left').addClass('fa-angle-right')
        $(self.base_left_part).addClass 'squash'
        $(self.base_right_part).addClass 'squash'

      else if squash is false
        $(self.base_squash_button).removeClass('fa-angle-right').addClass('fa-angle-left')
        $(self.base_left_part).removeClass 'squash'
        $(self.base_right_part).removeClass 'squash'

    else
      if squash is true
        $(self.base_squash_button).removeClass('fa-angle-right').addClass('fa-angle-left')
        $(self.base_left_part).removeClass 'squash'
        $(self.base_right_part).removeClass 'squash'
        save_ls "squash", false

      else if squash is false
        $(self.base_squash_button).removeClass('fa-angle-left').addClass('fa-angle-right')
        $(self.base_left_part).addClass 'squash'
        $(self.base_right_part).addClass 'squash'
        save_ls "squash", true
    return

