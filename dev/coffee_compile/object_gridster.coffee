Gridster =
  stop: (event, ui, widget) ->
    el = widget[0]
    id = el.getAttribute 'data-id'
    object_name = el.getAttribute 'data-object'
    item = window[object_name].collection[id]
    size_x = parseInt el.getAttribute 'data-sizex'
    size_y = parseInt el.getAttribute 'data-sizey'
    item.psize.sizex = size_x
    item.psize.sizey = size_y
    item.save()
    return

  init:
    configure:
      items: ->
        app['gridster'] = app['gridster'] or {}
        app['gridster']['items'] = $('[data-container*="items_all"] > ul').gridster
          widget_margins: [5, 5]
          widget_base_dimensions: [100, 100]
          resize:
            enabled: true
            stop: (event, ui, widget) ->
              Gridster.stop event, ui, widget
              return

            max_size: [Infinity, Infinity]
            min_size: [1, 1]

          draggable:
            handle: '.header'

        .data('gridster')

        for key, item of items.collection
          item.render()
        return

      tools: ->
        app['gridster']['tools'] = $('[data-container*="tools_all"] > ul').gridster
          widget_margins: [5, 5]
          widget_base_dimensions: [100, 100]
          resize:
            enabled: true
            stop: (event, ui, widget) ->
              Gridster.stop event, ui, widget
              return

            max_size: [Infinity, Infinity]
            min_size: [1, 1]

          draggable:
            handle: '.header'

        .data('gridster')

        for key, item of tools.collection
          item.render()
        return

      events: ->
        app['gridster']['events'] = $('[data-container*="events_all"] > ul').gridster
          widget_margins: [5, 5]
          widget_base_dimensions: [100, 100]
          resize:
            enabled: true
            stop: (event, ui, widget) ->
              Gridster.stop event, ui, widget
              return

            max_size: [Infinity, Infinity]
            min_size: [1, 1]

          draggable:
            handle: '.header'

        .data('gridster')

        for key, item of events.collection
          item.render()
        return

      sensors: ->
        app['gridster']['sensors'] = $('[data-container*="sensors_all"] > ul').gridster
          widget_margins: [5, 5]
          widget_base_dimensions: [100, 100]
          resize:
            enabled: true
            stop: (event, ui, widget) ->
              Gridster.stop event, ui, widget
              return

            max_size: [Infinity, Infinity]
            min_size: [1, 1]

          draggable:
            handle: '.header'

        .data('gridster')

        for key, item of sensors.collection
          item.render()
        return

