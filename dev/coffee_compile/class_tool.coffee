class Tool
  constructor: (options) ->
    options    = options ? {}
    @id        = options.id ? null
    @item_id   = options.item_id ? null
    @type      = options.type ? null
    @name      = options.name ? null
    @desc      = options.desc ? null
    @onoff     = options.onoff ? false
    @val       = options.val ? null
    @min       = options.min ? null
    @max       = options.max ? null
    @step      = options.step ? null
    @coherence = options.coherence ? null
    @psize     = options.psize ? sizex: 2, sizey: 1
    @list_params = ['id', 'item_id', 'type', 'name', 'desc', 'onoff', 'val', 'min', 'max', 'step', 'psize', 'coherence']

  init: (object) ->
    self = @
    object = object or {}

    for key, value of object when @list_params.indexOf(key) >= 0
      self[key] = value

    if @id is '' or @id is undefined or @id is null
      @set_id()

    if tools.collection[@id] is undefined
      tools.collection[@id] = @
    true

  set_id: ->
    if @id is '' or @id is undefined or @id is null
      @id = gen_time_hash()
      @save()
    true

  to_save_params: ->
    self = @
    params = {}

    for key, value of @ when @list_params.indexOf(key) >= 0
      params[key] = value
    params

  save: ->
    self = @
    tools.collection[self.id] = @
    params = JSON.stringify @to_save_params()

    $.ajax
      url: routes.path("tool_save", @)
      type: routes["tool_save"].type
      data: data: params
      success: (data) ->
        return
      error: (data, status, error) ->
        return
    true

  update: ->
    self = @
    tools.collection[@id] = @
    params = JSON.stringify @to_save_params()

    $.ajax
      url: routes.path("tool_update", self)
      type: routes["tool_update"].type
      data: data: params
      success: (data) ->
        return
      error: (data, status, error) ->
        return
    true

  delete: ->
    self = @

    $.ajax
      url: routes.path("tool_delete", @)
      type: routes["tool_delete"].type
      success: (data) ->
        #$('[data-item="' + self.id + '"]').fadeOut 'normal', ->
          #$('[data-item="' + self.id + '"]').remove()
          #return
        delete tools.collection[@id]
        return
      error: (data, status, error) ->
        return
    true

  render: ->
    self = @
    raw = $(tools.template[@type].html)[0]
    $(raw).attr 'data-id', @id
    $(raw).find('[data-id]').attr 'data-id', @id

    $(raw).find('[data-name]')
      .attr 'data-name', @name
      .attr 'data-id', @id
      .text @name

    $(raw).find('[data-desc]')
      .attr 'data-desc', @desc
      .attr 'data-id', @id
      .text @desc

    $(raw).find('input[type="checkbox"]')
      .attr 'id', @id
      .next().attr 'for', @id

    app.gridster.tools.add_widget raw, @psize.sizex, @psize.sizey

    #raw = $(window.tools.template[self.type].html)[0]

    #$(raw).find('[data-status]').attr 'data-status', "tools"
    #$(raw).find('.switch').attr 'id', @id
    #$(raw).find('label').attr 'for', @id
    #$(raw).find('.range').attr 'id', @id
    #$(raw).find('[data-id]').attr 'data-id', @id
    #$(raw).find('[role="tool name"]').text @name or "-------"
    #$(raw).find('[role="tool desc"]').text @desc

    #$(raw).find('[data-write="tools temp name"]').val @name
    #$(raw).find('[data-write="tools temp desc"]').val @desc

    #$(raw).find('input[type="checkbox"]').prop("checked", @onoff)
    #$(raw).find('input[type="range"]').val @val

    #$(raw).draggable
      #revert: "invalid"
      #start: (e, el) ->
        #target = e.target
        #$(target).addClass('top_all')
        #return

      #stop: (e, el) ->
        #target = e.target
        #$(target).removeClass('top_all')
        #return

    #$('.body__' + 'tools' + '[data-id="' + @item_id + '"]').append raw
    return

  #render_in_part: ->
    #self = @
    #raw = $(window.tools.template[self.type].html)[0]

    #$(raw).find('[data-status]').attr 'data-status', "tools"
    #$(raw).find('.switch').attr 'id', @id
    #$(raw).find('label').attr 'for', @id
    #$(raw).find('.range').attr 'id', @id
    #$(raw).find('[data-id]').attr 'data-id', @id
    #$(raw).find('[role="tool name"]').text @name or "-------"
    #$(raw).find('[role="tool desc"]').text @desc

    #$(raw).find('[data-write="tools temp name"]').val @name
    #$(raw).find('[data-write="tools temp desc"]').val @desc

    #$(raw).find('input[type="checkbox"]').prop("checked", @onoff)
    #$(raw).find('input[type="range"]').val @val
    #$(raw).attr('style', 'display: inline-block;')

    #$(raw).draggable
      #revert: "invalid"
      #start: (e, el) ->
        #target = e.target
        #$(target).addClass('top_all')
        #return

      #stop: (e, el) ->
        #target = e.target
        #$(target).removeClass('top_all')
        #return

    #$('.tools_part').append raw

    #return

  #render_for_events: (item_name) ->
    #self = @
    #raw = $(window.tools.template[self.type].html)[0]

    #$(raw).find('[data-status]').attr 'data-status', "events"
    #$(raw).find('.switch').attr 'id', "e:" + @id
    #$(raw).find('label').attr 'for', "e:" + @id
    #$(raw).find('.range').attr 'id', "e:" + @id
    #$(raw).find('[data-id]').attr 'data-id', @id
    #$(raw).find('[role="tool name"]').text @name
    #$(raw).find('[role="tool desc"]').text @desc

    #$(raw).find('[data-write="tools temp name"]').val @name
    #$(raw).find('[data-write="tools temp desc"]').val @desc

    #$(raw).find('input[type="checkbox"]').prop("checked", @onoff)
    #$(raw).find('input[type="range"]').val @val
    #$(raw).attr('style', 'display: inline-block;')

    #$(raw).draggable
      #revert: "invalid"
      #start: (e, el) ->
        #target = e.target
        #$(target).addClass('top_all')
        #return

      #stop: (e, el) ->
        #target = e.target
        #$(target).removeClass('top_all')
        #return

    #$('#events_tools_elements').append raw
    #return
