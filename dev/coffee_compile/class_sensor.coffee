class Sensor
  constructor: (options) ->
    options       = options ? {}
    @id           = options.id ? null
    @item_id      = options.item_id ? null
    @name         = options.name ? null
    @desc         = options.desc ? null
    @val          = options.val ? "-"
    @type         = options.type ? "thermometer"
    @frequency    = options.frequency ? 30
    @value_low    = options.value_low ? [0, 10]
    @value_normal = options.value_normal ? [11, 20]
    @value_high   = options.value_high ? [21, 30]
    @coherence    = options.coherence ? null
    @psize        = options.psize ? sizex: 2, sizey: 2
    @list_params  = ['id', 'name', 'desc', 'item_id', 'type', 'frequency', 'value_low', 'value_normal', 'value_high', 'coherence', 'psize']

  init: (object) ->
    self = @
    object = object or {}

    for key, value of object when @list_params.indexOf(key) >= 0
      self[key] = value

    if @id is '' or @id is undefined or @id is null
      @set_id()

    if sensors.collection[@id] is undefined
      sensors.collection[@id] = @
    true

  set_id: ->
    self = @
    if @id is '' or @id is undefined or @id is null
      @id = gen_time_hash()
      @save()
    true

  to_save_params: ->
    self = @
    params = {}

    for key, value of @ when @list_params.indexOf(key) >= 0
      params[key] = value
    params

  save: ->
    self = @
    sensors.collection[@id] = @
    params = JSON.stringify @to_save_params()

    $.ajax
      url: routes.path("sensor_save", @)
      type: routes["sensor_save"].type
      data: data: params
      success: (data) ->
        return
      error: (data, status, error) ->
        return
    true

  update: ->
    self = @
    sensors.collection[@id] = @
    params = JSON.stringify @to_save_params()

    if sensors.collection.hasOwnProperty( @id ) is true
      $.ajax
        url: routes.path("sensor_update", @)
        type: routes["sensor_update"].type
        data: data: params
        success: (data) ->
          return
        error: (data, status, error) ->
          return
    true

  delete: ->
    self = @
    $.ajax
      url: routes.path("sensor_delete", @)
      type: routes["sensor_delete"].type
      success: (data) ->
        $('[data-item="' + self.id + '"]').fadeOut 'normal', ->
          $('[data-item="' + self.id + '"]').remove()
          return
        delete sensors.collection[@id]
        return
      error: (data, status, error) ->
        return
    true

  # FIXME - what is
  get_data_run: ->
    if @frequency isnt null and @frequency isnt undefined and @frequency isnt "" and parseInt(@frequency) > 0
      times = parseInt(@frequency) * 1000
      sensors.timers[@id] = setTimeout (->
        @get_data()
        return
      ).bind(@), times
    return

  # FIXME - what is
  get_data: ->
    self = @
    $.ajax
      url: routes.path("sensor_data", @)
      type: routes["sensor_data"].type
      cache: false
      success: (data) ->
        $('.body__tool[data-id="' + self.id + '"]').addClass('get_data')
        self.set_value data
        return
      error: (data, status, error) ->
        return
    return

  # FIXME - what is
  get_data_stop: ->
    clearTimeout sensors.timers[@id]

  # FIXME - what is
  set_value: (value) ->
    @val = value
    $('[data-id="' + @id + '"][role="sensor value"]').text(@val)
    el = $('.body__tool[data-id="' + @id + '"]')[0]
    remove_class_with_pause el, 'get_data'
    @get_data_run()
    @check_value()
    return

  # FIXME - what is
  check_value: ->
    current = parseFloat(@val)
    el = $('.body__tool[data-id="' + @id + '"]')[0]

    if current and typeof(current) is 'number'
      if @value_low[0] <= current <= @value_low[1]
        $(el).removeClass('volume_high volume_normal')
        $(el).addClass('volume_low')

      else if @value_normal[0] <= current <= @value_normal[1]
        $(el).removeClass('volume_low volume_high')

      else if @value_high[0] <= current <= @value_high[1]
        $(el).removeClass('volume_low volume_normal')
        $(el).addClass('volume_high')

    return

  render: ->
    self = @
    raw = $(sensors.template[@type].html)[0]
    $(raw).attr 'data-id', @id
    $(raw).find('[data-id]').attr 'data-id', @id

    $(raw).find('[data-name]')
      .attr 'data-name', @name
      .attr 'data-id', @id
      .text @name

    $(raw).find('[data-desc]')
      .attr 'data-desc', @desc
      .attr 'data-id', @id
      .text @desc

    app.gridster.sensors.add_widget raw, @psize.sizex, @psize.sizey
    return

