class Item
  constructor: (options) ->
    options  = options ? {}
    @id      = options.id ? null
    @name    = options.name ? null
    @desc    = options.desc ? null
    @sensors = options.sensors ? []
    @tools   = options.tools ? []
    @psize   = options.psize ? sizex: 2, sizey: 2
    @list_params = ['id', 'name', 'desc', 'sensors', 'tools', 'psize']

  init: (object) ->
    self = @
    object = object or {}

    for key, value of object when @list_params.indexOf(key) >= 0
      self[key] = value

    if @id is '' or @id is undefined or @id is null
      @set_id()

    if items.collection[@id] is undefined
      items.collection[@id] = @
    true

  set_id: ->
    self = @
    if @id is '' or @id is undefined or @id is null
      @id = gen_time_hash()
      @save()
    true

  to_save_params: ->
    self = @
    params = {}

    for key, value of @ when @list_params.indexOf(key) >= 0
      params[key] = value
    params

  save: ->
    self = @
    items.collection[@id] = @
    params = JSON.stringify @to_save_params()

    $.ajax
      url: routes.path("item_save", @)
      type: routes["item_save"].type
      data: data: params
      success: (data) ->
        return
      error: (data, status, error) ->
        return
    true

  update: ->
    self = @
    items.collection[@id] = @
    params = JSON.stringify @to_save_params()

    $.ajax
      url: routes.path("item_update", @)
      type: routes["item_update"].type
      data: data: params
      success: (data) ->
        return
      error: (data, status, error) ->
        return
    true

  delete: ->
    self = @
    $.ajax
      url: routes.path("item_delete", @)
      type: routes["item_delete"].type
      success: (data) ->
        # FIX REMOVE DIV FOR ITEM
        delete items.collection[@id]
        return

      error: (data, status, error) ->
        return
    true

  add_tool: (params) ->
    self = @
    params = params or {}
    tool = new Tool()
    tool.init(params)
    tool.save()
    tool.render()

    @tools.push tool.id
    @update()
    true

  add_sensor: (params) ->
    self = @
    params = params or {}
    sensor = new Sensor()
    sensor.init(params)
    sensor.save()
    sensor.render()

    @sensors.push sensor.id
    @update()
    true

  render: ->
    self = @
    raw = $(items.template.main.html)[0]
    $(raw).attr 'data-id', @id
    $(raw).find('[data-id]').attr 'data-id', @id

    $(raw).find('[data-name]')
      .attr 'data-name', @name
      .attr 'data-id', @id
      .text @name

    $(raw).find('[data-desc]')
      .attr 'data-desc', @desc
      .attr 'data-id', @id
      .text @desc

    app.gridster.items.add_widget raw, @psize.sizex, @psize.sizey

    #console.log "raw:", raw
    #TODO надо дописать функцию удаления и добавления tool и sensor, а так же draganddrop для всего этого
    return

  #render: ->
    #self = @
    #raw = $(items.template.main.html)[0]
    #$(raw).find('[data-id]').attr 'data-id', @id
    #$(raw).find('[data-item]').attr 'data-item', @id
    #$(raw).find('[data-field="name"]').text @name
    #$(raw).find('[data-field="desc"]').text @desc
    #$(raw).find('.body__tools').droppable
      #drop: (event, el) ->
        #target = event.target
        #dropped = el.draggable[0]

        #item_id = $(target).attr('data-id')
        #tool_id = $(dropped).children().attr('data-id')

        #$(target).append dropped
        #$(dropped).attr 'style', 'position: relative;'

        #tool = tools.collection[tool_id]
        #tool.item_id = item_id
        #tool.save()
        #return

      #accept: (drop) ->
        #type = $(drop[0]).children().attr('data-drag')
        #if type is 'tool' then true else false

      #hoverClass: "ui-state-hover"

    #$(raw).find('.body__sensors').droppable
      #drop: (event, el) ->
        #target = event.target
        #dropped = el.draggable[0]

        #item_id = $(target).attr('data-id')
        #tool_id = $(dropped).children().attr('data-id')

        #$(target).append dropped
        #$(dropped).attr 'style', 'position: relative;'

        #sensor = sensors.collection[tool_id]
        #sensor.item_id = item_id
        #sensor.save()
        #return

      #accept: (drop) ->
        #type = $(drop[0]).children().attr('data-drag')
        #if type is 'sensor' then true else false

      #hoverClass: "ui-state-hover"

    ## FIXME current_column
    #$('[role="item column ' + window.current_column + '"]').append raw
    #append_tools self.id
    #append_sensors self.id
    #count_row_column()

    #if self.tools.length > 0
      #self.tools.forEach (el) ->
        #_t =
          #id: el

        #$.ajax
          #url: routes.path("tool_get", _t)
          #type: routes["tool_get"].type
          #dataType: "json"
          #cache: false
          #success: (data) ->
            #tool = new Tool
            #tool.init data
            #tool.render()
            #return
          #error: (data, status, error) ->
            #return
        #return

    #if self.sensors.length > 0
      #self.sensors.forEach (el) ->
        #_t =
          #id: el

        #$.ajax
          #url: routes.path("sensor_get", _t)
          #type: routes["sensor_get"].type
          #dataType: "json"
          #cache: false
          #success: (data) ->
            #tool = new Sensor
            #tool.init data
            #tool.render()
            #return
          #error: (data, status, error) ->
            #return
        #return
    #return

