$ ->
  ls = window.ls = window.localStorage
  Journal.init $('[data-container="journal"]')[0]
  LeftMenu.init '[data-area="left_menu"]'
  APP_SMART_HOME.init()
  APP_SMART_HOME.init_ajax()
  APP_SMART_HOME.init_global_event()
  return
