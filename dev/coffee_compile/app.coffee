resize_item = (el) ->
  #console.log "parent:", el
  childrens = $(el).children().get()
  base_width = $(el).outerWidth()
  base_height = $(el).outerHeight()
  size_x = parseInt $(el).attr 'data-sizex'
  size_y = parseInt $(el).attr 'data-sizey'
  total_width = 0
  total_height = 0

  for i in childrens
    #console.log "C:", i
    if total_width < $(i).outerWidth() then total_width = $(i).outerWidth()
    total_height += $(i).outerHeight()

  if total_height > base_height
    app.gridster.items.resize_widget $(el), size_x, ++size_y

  #else if total_height < base_height
    #app.gridster.e.resize_widget $(el), size_x, --size_y
    #console.log "--:", total_height, ":::", base_height
  return

routes = window.routes =
  init: (data, cb) ->
    data.forEach (item) ->
      routes[item.name] = {}
      routes[item.name]['name'] = item.name
      routes[item.name]['path'] = item.path
      routes[item.name]['type'] = item.type
      return

    if cb
      cb()
    true

  path: (_path, _object) ->
    if routes.hasOwnProperty(_path) is true
      path = routes[_path]['path']
      if /:\w+/.test(path) is true and _object isnt undefined
        args = Object.keys _object

        _t = path.split('/')
        _m = []
        _t.forEach (el) ->
          if /:\w+/.test(el) is true
            _el = el.replace(":", "")
            if args.indexOf(_el) >= 0
              _m.push _object[_el]
          else
            _m.push el
          return
        result = _m.join("/")
      else
        result = routes[_path]['path']
    else
      result = null
    return result


APP_SMART_HOME = ASH = app_smart_home = ash = app = window.APP_SMART_HOME = window.ASH = window.app_smart_home = window.ash = window.app =
  init_ajax: ->
    $.ajax
      url: "/json/settings.json"
      type: "GET"
      cache: false
      dataType: "json"
      success: (data, status, err) ->
        # TODO перенести проверку наличия settings в другое место
        if ls['settings'] isnt undefined
          window.settings = settings = get_ls 'settings'

        else
          window.settings = settings =
            sensors:
              status: false

          save_ls 'settings', settings
        # ######

        # FIXME settings
        #$('input[data-id="settings_sensor_on_off"]').prop("checked", settings.sensors.status)
        #$('[role="settings sensors_label"]').text gen_label(settings.sensors.status)
        # TODO reconfigure this function

        ASH.push_setting data.templates.items,   "items"
        ASH.push_setting data.templates.tools,   "tools"
        ASH.push_setting data.templates.sensors, "sensors"
        ASH.push_setting data.templates.events,  "events"

        routes.init data.routes

        ASH.init_env()
        true

      error: (data, status, error) ->
        false
    true

  push_setting: (_object, _name, cb) ->
    window[_name] =
      get: ->
        _object.forEach (el) ->
          $.ajax
            url: el.template
            type: 'GET'
            cache: false
            dataType: 'html'
            async: false
            success: (data) ->
              window[_name].template[el.name] = {}
              window[_name].template[el.name]["label"] = el.label
              window[_name].template[el.name]["html"] = data

              if el.type
                window[_name].template[el.name]["type"] = el.type

              window[_name].timers = {}
              return

            error: (data, status, error) ->
              return
          return
        return

      template: {}
      tempdata: {}
      collection: {}

    window[_name].get()

    if cb
      cb()
    return

  init_env: ->
    $.ajax
      url: routes["items_get"]["path"]
      type: routes["items_get"]["type"]
      dataType: "json"
      cache: false
      success: (data) ->
        data.forEach (el) ->
          item = new Item
          item.init el
          return
        return

      error: (data, status, error) ->
        return

    $.ajax
      url: routes["tools_get"]["path"]
      type: routes["tools_get"]["type"]
      dataType: "json"
      cache: false
      success: (data) ->
        data.forEach (el) ->
          item = new Tool
          item.init el
          return
        return

      error: (data, status, error) ->
        return

    $.ajax
      url: routes["sensors_get"]["path"]
      type: routes["sensors_get"]["type"]
      dataType: "json"
      cache: false
      success: (data) ->
        data.forEach (el) ->
          item = new Sensor
          item.init el
          return
        return

      error: (data, status, error) ->
        return

    $.ajax
      url: routes["events_get"]["path"]
      type: routes["events_get"]["type"]
      dataType: "json"
      cache: false
      success: (data) ->
        data.forEach (el) ->
          item = new Event
          item.init el
          return
        return

      error: (data, status, error) ->
        return
    return

  erase: (object) ->
    for key, value of object
      object[key] = null
    true

  init_global_event: ->
    $('[data-area="configure"],[data-area="logics"],[data-area="modal"],[data-area="alert"]').on 'click keyup submit change dblclick contextmenu', (event) ->
      event = event or window.event
      target = event.target or event.srcElement
      parent = $(target).parent()
      type_event = event.type
      waction = target.attributes['w-action']

      console.log "key code:", event.keyup
      console.log "event:", type_event, 'target:', target
      console.log "input:", $(target).attr('data-input')

      action = if $(target).attr('data-action') then $(target).attr('data-action') else $(parent).attr('data-action')
      if action
        console.log "action:", action
        app.do_action action, event, target

      if type_event is 'click' and $(target).attr('data-input') is "true"
        id = $(target).attr 'data-id'
        temp_id = gen_time_hash()
        object_name = $(target).attr 'data-object'
        object_field = $(target).attr 'data-field'

        input = gen_input
          type: 'text'
          value: $(target).text()
          attrs:
            'for-id'      : temp_id
            'data-id'     : id
            'data-object' : object_name
            'data-field'  : object_field

        prev = $(target).prev()
        $(target)
          .attr('data-input-id', temp_id)
          .hide()

        $(input)
          .insertAfter(prev)
          .focus()

      if (type_event is 'change' or event.keyCode is 13) and $(target).attr('for-id')
        temp_id = $(target).attr 'for-id'
        id = $(target).attr 'data-id'
        object_name = $(target).attr 'data-object'
        object_field = $(target).attr 'data-field'

        item = window[object_name].collection[id]

        field = $('[data-input-id=' + temp_id + ']')[0]
        field.removeAttribute 'data-input-id'
        $(field).text $(target).val()

        item[object_field] = $(target).val()

        $(target).remove()
        $(field).show()

        if object_name is 'items'
          resize_item $(field).parent()[0]

        item.save()
        console.log "item:", item

      if type_event is 'contextmenu'
        event.preventDefault()
        id = target.attributes['data-id'].value
        object = target.attributes['data-object'].value

        if id isnt undefined and id isnt "" and object isnt undefined and object isnt ""
          Widget.items_modal.set id: id
          Widget.items_modal.render()

      if waction isnt undefined and waction isnt ""
        console.log "waction:", waction, "target:", target
        waction = waction.value
        wid = $(target).attr 'w-id'

        if waction is 'close'
          Widget.items_modal.close(wid)
    return

  do_action: (action, event, target) ->
    if /add new item/.test(action) is true and event.type is 'click'
      item = new Item
        id: gen_time_hash()
        name: 'Имя'
        desc: 'Описание'

      item.init()
      item.save()
      item.render()

    if /add new event/.test(action) is true and event.type is 'click'
      item = new Event
        id: gen_time_hash()
        name: 'Имя'
        desc: 'Описание'

      item.init()
      item.save()
      item.render()
    return

  init: ->
    app.gridster = {}
    return

  _init: ->
    $('[data-content="configure"],[data-content="logics"],[data-content="settings"]').on 'click keyup submit change dblclick', (event) ->
      event = event or window.event
      target = event.target or event.srcElement
      parent = $(target).parent()
      type_event = event.type

      if type_event is 'submit'
        event.preventDefault()
        event.stopPropagation()

      #console.log "target:", target
      #console.log "type_event:", type_event

      action = if $(target).attr('data-action') then $(target).attr('data-action') else $(parent).attr('data-action')
      write  = if $(target).attr('data-write') then $(target).attr('data-write')
      set    = if $(target).attr('data-set') then $(target).attr('data-set')
      ce     = if $(target).attr('contenteditable') is "true" then $(target).attr('data-type') else undefined

      if write
        _item = write.split ' '
        _form_name = $(target).attr('data-for-form')
        ASH.write _item, event

      if action
        _action = action.split ' '
        _verb = _action[0]
        _item = _action[1]
        ASH.action _verb, _item, target, event

      if set
        _type = set
        ASH.touch _type, event, target

      if ce
        ASH.write_ce ce, event, target

      return

  action: (_verb, _item, target, event) ->
    console.log "_verb:", _verb, "_item:", _item

    if /menu/.test(_verb) is true and /open/.test(_item)
      $(target).next().toggle()

    if /show/.test(_verb) is true and /form/.test(_item) is true
      form_div = $('[data-form="' + _item + '"]')
      $(form_div).parent().show()
      if $(form_div).is(':hidden')
        $(form_div).fadeIn()

    if /close/.test(_verb) is true and /form/.test(_item) is true
      $('[data-form="' + _item + '"]').fadeOut().parent().fadeOut()

    if /add/.test(_verb) is true and /^item$/.test(_item) is true
      event.preventDefault()
      event.stopPropagation()
      _form_name = $(target).attr('data-form')
      button = $('button[data-for-form="' + _form_name + '"]')[0]

      item = new Item
      item.init items.temp
      item.save()
      item.render()

      ASH.set_button_disabled(button, true)

    if /add/.test(_verb) is true and /^event$/.test(_item) is true
      button = $('button[data-for-form="' + _form_name + '"]')[0]

      item = new Event
      item.init events.temp
      item.save()
      item.render()

      ASH.set_button_disabled(button, true)

    if /add/.test(_verb) is true and /^to_item/.test(_item) is true
      event.preventDefault()
      event.stopPropagation()

      type = _item.split('_')[2]
      name = $(target).attr('data-tool-type')
      item_id = $(target).attr('data-id')

      _params =
        type: name
        item_id: item_id

      item = items.collection[item_id]
      if type is 'tools'
        item.add_tool(_params)
      else if type is 'sensors'
        item.add_sensor(_params)

    if /delete/.test(_verb) is true and /item/.test(_item) is true
      id = $(target).attr('data-id')
      item = items.collection[id]
      item.delete()

    if /tool/.test(_verb) is true and /desc_edit_form_show/.test(_item) is true and event.type is 'dblclick'
      id = $(target).attr('data-id')
      form = $('form[data-id="' + id + '"]')
      $(form).toggle()

    if /tool/.test(_verb) is true and /update/.test(_item) is true and event.type is 'submit'
      id = $(target).attr('data-id')
      button = $('button[data-id="' + id + '"]')[0]
      tool = tools.collection[id]
      tool.init tools.temp
      tool.update()
      ASH.erase tools.temp
      ASH.set_button_disabled(button, true)

    if /tool/.test(_verb) is true and /toggle/.test(_item) is true and event.type is 'change'
      id = $(target).attr('data-id')
      tool = tools.collection[id]
      onoff = $(target).prop('checked')
      tools.temp.onoff = onoff
      tool.init tools.temp
      tool.update()

    if /tool/.test(_verb) is true and /push_status/.test(_item) is true and event.type is 'change'
      type = $(target).attr('input-type')
      data_id = $(target).attr('data-id')

      tool = tools.collection[data_id]

      if type is 'checkbox'
        val = $(target).prop('checked')
        tool.onoff = val

      else if type is 'range'
        val = $(target).val()
        tool.val = val

      tool.save()

    if /sensor/.test(_verb) is true and /desc_edit_form_show/.test(_item) is true and event.type is 'dblclick'
      id = $(target).attr('data-id')
      form = $('form[data-id="' + id + '"]')
      $(form).toggle()

    if /sensor/.test(_verb) is true and /update/.test(_item) is true and event.type is 'submit'
      id = $(target).attr('data-id')
      button = $('button[data-id="' + id + '"]')[0]
      sensor = sensors.collection[id]
      sensor.init sensors.temp
      sensor.update()
      ASH.erase sensors.temp
      ASH.set_button_disabled(button, true)

    if /settings/.test(_verb) is true and /sensors/.test(_item) is true and event.type is 'change'
      settings.sensors.status = $(target).is(':checked')
      save_ls 'settings', settings
      keys = Object.keys(sensors.collection)

      # FIXME gen_label
      # $('[role="settings sensors_label"]').text gen_label(settings.sensors.status)

      if settings.sensors.status is true
        keys.forEach (el) ->
          sensors.collection[el].get_data_run()
          return

      else if settings.sensors.status is false
        keys.forEach (el) ->
          sensors.collection[el].get_data_stop()
          return

    return

  write: (_item, event) ->
    type = _item[0]
    attr = _item[1]
    name = _item[2]
    value = event.target.value
    id = $(event.target).attr('data-id')
    button = $('button[data-id="' + id + '"]')[0]
    if button is undefined
      for_form = $(event.target).attr('data-for-form')
      button = $('button[data-for-form="' + for_form + '"]')[0]

    window[type][attr][name] = value

    if $('[role="' + type.remove_s() + ' ' + name + '"][data-id="' + id + '"]') isnt undefined and event.type is "keyup"
      $('[role="' + type.remove_s() + ' ' + name + '"][data-id="' + id + '"]').text value

    if window[type][attr][name] isnt ''
      ASH.set_button_disabled(button, false)
    else if window[type][attr][name] is ''
      ASH.set_button_disabled(button, true)

    return

  touch: (_type, event, target) ->
    _type = _type
    _id = $(target).attr('data-id')
    item = window[_type]['collection'][_id]
    $('[data-field="set ' + _type + ' name"]').text item.name
    $('[data-type="' + _type + '"][data-id]').removeClass 'flag'
    $('[data-type="' + _type + '"][data-id="' + _id + '"]').addClass 'flag'
    return

  write_ce: (_item, event, target) ->
    if event.type is 'keyup'
      _item = _item.split ' '
      _type = _item[0]
      _field = _item[1]
      _id = $(target).attr 'data-id'
      _value = $(target).text()

      if window[_type]['collection'][_id]['save_timeout'] isnt undefined
        clearTimeout window[_type]['collection'][_id]['save_timeout']

      item = window[_type]['collection'][_id]
      item[_field] = _value
      save_with_pause item, _type, 3000

    else if event.type is 'click'
      console.log "click:", _item

    return

  set_button_disabled: (button, status) ->
    if status is true
      $(button).addClass 'pure-button-disabled'
    else if status is false
      $(button).removeClass 'pure-button-disabled'

    $(button).prop 'disabled', status
    return
