redis  = require 'redis'
config = require './config'

get_client = ->
  client = redis.createClient 6379, 'localhost'
  client.select config.dev_database
  client

module.exports =
  get_client: get_client
