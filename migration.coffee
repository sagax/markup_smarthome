db_access = require './db_access'

save_el = (el, id, name) ->
  client = db_access.get_client()
  client.set [name, ':', id].join(''), JSON.stringify(el)
  client.quit()
  return

items = [
  id      : 1
  name    : "Кухня"
  desc    : "Кухня в первом доме"
  sensors : []
  tools   : []
,
  id      : 2
  name    : "Спальня"
  desc    : "Спальня в первом доме"
  sensors : []
  tools   : []
,
  id      : 3
  name    : "Ванна"
  desc    : "Ванна в первом доме"
  sensors : []
  tools   : []
]

for i, el of items
  save_el el, el.id, 'item'

items = [
  id: 0
  name: 'Включатель'
  desc: 'Включатель в кухне'
  item_id: null
  type: 'switch'
  onoff: false
  val: null
  min: null
  max: null
  step: null
  coherence: null
,
  id: 1
  name: 'Регулятор'
  desc: 'Регулятор температуры в кухне'
  item_id: null
  type: 'range'
  onoff: null
  val: 35
  min: 0
  max: 100
  step: 5
  coherence: null
,
  id: 3
  name: 'Включатель 2'
  desc: 'Включатель в спальне'
  item_id: null
  type: 'switch'
  onoff: true
  val: null
  min: null
  max: null
  step: null
  coherence: null
]

for i, el of items
  save_el el, el.id, 'tool'

items = [
  id: 0
  item_id: null
  name: 'Температура'
  desc: 'температура в кухне'
  onoff: false
  type: 'thermometer'
  val: 10
  value_low: [0, 10]
  value_normal: [11, 20]
  value_high: [21, 30]
  coherence: null
,
  id: 1
  item_id: null
  name: 'Влажность'
  desc: 'Влажность в кухне'
  onoff: false
  type: 'barometer'
  val: 21
  value_low: [0, 10]
  value_normal: [11, 20]
  value_high: [21, 30]
  coherence: null
,
  id: 2
  item_id: null
  name: 'Сухость'
  desc: 'Сухость в кухне'
  onoff: false
  type: 'dryness'
  val: 7
  value_low: [0, 10]
  value_normal: [11, 20]
  value_high: [21, 30]
  coherence: null
]

for i, el of items
  save_el el, el.id, 'sensor'

items = [
  id: 1
  name: 'Правило 1'
  desc: 'Описание правила 1'
  condition_ids: []
  reaction_ids: []
,
  id: 2
  name: 'Правило 2'
  desc: 'Описание правила 2'
  condition_ids: []
  reaction_ids: []
,
  id: 3
  name: 'Правило 3'
  desc: 'Описание правила 3'
  condition_ids: []
  reaction_ids: []
]

for i, el of items
  save_el el, el.id, 'event'
