# MARKUP SMART HOME WEB APPLICATION

## Верстка

* Элементы с аттрибутом [data-action] могут иметь элементы с уровнем вложенности не больше чем 1. Например:

``` html
<a href="#" class="pure-menu-link" data-action="add form item">
  <i class="fa fa-plus-square fa-2x"></i>
</a>
```

В данном случае <i> вложен в <a> - правильно. Иметь еще что-то в <i> - неправильно.


