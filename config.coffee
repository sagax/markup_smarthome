path = require 'path'

root = __dirname

dev_path               = path.join root,     'dev'
dev_path_js            = path.join dev_path, 'js'
dev_path_coffee        = path.join dev_path, 'coffee'
dev_path_coffee_compile = path.join dev_path, 'coffee_compile'
dev_path_json          = path.join dev_path, 'json'
dev_path_fonts         = path.join dev_path, 'fonts'
dev_path_favicon       = path.join dev_path, 'favicon'
dev_path_template_part = path.join dev_path, 'template'
dev_path_css           = path.join dev_path, 'css'
dev_path_sass          = path.join dev_path, 'sass'
dev_path_img           = path.join dev_path, 'img'
dev_path_ejs           = path.join dev_path, 'ejs'
dev_path_old           = path.join dev_path, 'old'
dev_path_doc           = path.join dev_path, 'doc'

prod_path      = path.join root,      'prod'
prod_path_js   = path.join prod_path, 'js'
prod_path_json = path.join prod_path, 'json'
prod_path_css  = path.join prod_path, 'css'
prod_path_img  = path.join prod_path, 'img'
prod_path_doc  = path.join prod_path, 'doc'

dev_database = 2

module.exports =
  root: root

  dev_path:               dev_path
  dev_path_js:            dev_path_js
  dev_path_json:          dev_path_json
  dev_path_fonts:         dev_path_fonts
  dev_path_favicon:       dev_path_favicon
  dev_path_template_part: dev_path_template_part
  dev_path_coffee:        dev_path_coffee
  dev_path_coffee_compile: dev_path_coffee_compile
  dev_path_css:           dev_path_css
  dev_path_sass:          dev_path_sass
  dev_path_img:           dev_path_img
  dev_path_ejs:           dev_path_ejs
  dev_path_old:           dev_path_old
  dev_path_doc:           dev_path_doc

  prod_path:      prod_path
  prod_path_js:   prod_path_js
  prod_path_json: prod_path_json
  prod_path_css:  prod_path_css
  prod_path_img:  prod_path_img
  prod_path_doc:  prod_path_doc

  dev_database: dev_database
