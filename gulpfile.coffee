$        = (require 'gulp-load-plugins')()
config   = require './config.coffee'
del      = require 'del'
gulp     = require 'gulp'
gulpsync = $.sync gulp
path     = require 'path'
util     = require 'util'
htmlinc  = require 'gulp-htmlincluder'
mdhtml   = require 'gulp-markdown'
mpdf     = require 'gulp-markdown-pdf'
concat   = require 'gulp-coffeescript-concat'

mdhtml.marked.setOptions
  renderer: new mdhtml.marked.Renderer()
  gfm: true
  tables: true
  breaks: false
  pedantic: false
  sanitize: true
  smartLists: true
  smartypants: false

gulp.task 'clean', ->
  del config.prod_path,
    force: true

gulp.task 'copy_dir', ->
  gulp.src path.join(config.dev_path_img, '**/*'),     {base: config.dev_path}
    .pipe gulp.dest config.prod_path
  gulp.src path.join(config.dev_path_js, '**/*'),      {base: config.dev_path}
    .pipe gulp.dest config.prod_path
  gulp.src path.join(config.dev_path_css, '**/*'),     {base: config.dev_path}
    .pipe gulp.dest config.prod_path
  gulp.src path.join(config.dev_path_json, '**/*'),    {base: config.dev_path}
    .pipe gulp.dest config.prod_path
  gulp.src path.join(config.dev_path_fonts, '**/*'),   {base: config.dev_path}
    .pipe gulp.dest config.prod_path
  gulp.src path.join(config.dev_path_favicon, '**/*'), {base: config.dev_path}
    .pipe gulp.dest config.prod_path
  gulp.src path.join(config.dev_path_old, '**/*'),     {base: config.dev_path}
    .pipe gulp.dest config.prod_path
  return

gulp.task 'gen_js', ->
  gulp.src path.join config.dev_path_coffee, '*.coffee'
    .pipe $.coffee bare: true
    .pipe gulp.dest config.prod_path_js

gulp.task 'gen_js_livereload', ->
  gulp.src path.join config.dev_path_coffee, '*.coffee'
    .pipe $.coffee bare: true
    .pipe gulp.dest config.prod_path_js
    .pipe $.livereload()

gulp.task 'gen_coffee_compare', ->
  gulp.src path.join config.dev_path_coffee_compile, '*.coffee'
    .pipe concat 'main.coffee'
    .pipe $.coffee bare: true
    .pipe gulp.dest config.prod_path_js

gulp.task 'gen_coffee_compare_live', ->
  gulp.src path.join config.dev_path_coffee_compile, '*.coffee'
    .pipe concat 'main.coffee'
    .pipe $.coffee bare: true
    .pipe gulp.dest config.prod_path_js
    .pipe $.livereload()

gulp.task 'gen_css', ->
  gulp.src path.join(config.dev_path_sass, '*.sass')
    .pipe $.sass( indentedSyntax: true )
    .pipe $.autoprefixer
      browsers: [
        'Firefox ESR'
        'Firefox 14'
        'Opera 10'
        'last 2 version'
        'safari 5'
        'ie 8'
        'ie 9'
        'opera 12.1'
        'ios 6'
        'android 4'
      ]
      cascade: true
    .pipe gulp.dest config.prod_path_css

gulp.task 'gen_css_livereload', ->
  gulp.src path.join(config.dev_path_sass, '*.sass')
    .pipe $.sass( indentedSyntax: true )
    .pipe $.autoprefixer
      browsers: [
        'Firefox ESR'
        'Firefox 14'
        'Opera 10'
        'last 2 version'
        'safari 5'
        'ie 8'
        'ie 9'
        'opera 12.1'
        'ios 6'
        'android 4'
      ]
      cascade: true
    .pipe gulp.dest config.prod_path_css
    .pipe $.livereload()

gulp.task 'gen_html', ->
  gulp.src path.join config.dev_path, '*.ejs'
    .pipe $.ejs {env: ''}, ext: '.html'
    .pipe gulp.dest config.prod_path
  return

gulp.task 'gen_html_livereload', ->
  gulp.src path.join config.dev_path, '*.ejs'
    .pipe $.ejs {env: 'dev'}, ext: '.html'
    .pipe gulp.dest config.prod_path
    .pipe $.livereload()
  return

gulp.task 'gen_doc_md', ->
  gulp.src path.join config.dev_path_doc, '*.md'
    .pipe mdhtml()
    .pipe gulp.dest config.dev_path_doc
  return

gulp.task 'gen_doc_md_live', ->
  gulp.src path.join config.dev_path_doc, '*.md'
    .pipe mdhtml()
    .pipe gulp.dest config.dev_path_doc
    .pipe $.livereload()
  return

gulp.task 'gen_doc_pdf', ->
  options =
    #cssPath: path.join(config.prod_path_css, 'pdf.css')
    paperBorder: '1cm'

  gulp.src path.join config.dev_path_doc, '*.md'
    .pipe mpdf(options)
    .pipe gulp.dest config.prod_path_doc
  return

gulp.task 'gen_doc_html', ->
  gulp.src path.join config.dev_path_doc, '*.html'
    .pipe htmlinc()
    .pipe gulp.dest config.prod_path_doc
  return

gulp.task 'gen_doc_html_live', ->
  gulp.src path.join config.dev_path_doc, '*.html'
    .pipe htmlinc()
    .pipe gulp.dest config.prod_path_doc
    .pipe $.livereload()
  return

gulp.task 'gen_template_part', ->
  gulp.src path.join config.dev_path_template_part, '*.ejs'
    .pipe $.ejs {}, ext: '.html'
    .pipe gulp.dest config.prod_path
  return

gulp.task 'gen_template_part_live', ->
  gulp.src path.join config.dev_path_template_part, '*.ejs'
    .pipe $.ejs {}, ext: '.html'
    .pipe gulp.dest config.prod_path
    .pipe $.livereload()
  return

gulp.task 'copy_json_livereload', ->
  gulp.src path.join(config.dev_path_json, '**/*'), base: config.dev_path
    .pipe gulp.dest config.prod_path
    .pipe $.livereload()
  return

gulp.task 'watch', ->
  gulp.watch path.join(config.dev_path_sass, '*.sass'),             ['gen_css_livereload']
  gulp.watch path.join(config.dev_path_coffee_compile, '*.coffee'), ['gen_coffee_compare_live']
  gulp.watch path.join(config.dev_path_coffee, '*.coffee'),         ['gen_js_livereload']
  gulp.watch path.join(config.dev_path_json, '*.json'),             ['copy_json_livereload']
  gulp.watch path.join(config.dev_path, '*.ejs'),                   ['gen_html_livereload']
  gulp.watch path.join(config.dev_path_ejs, '*.ejs'),               ['gen_html_livereload']
  gulp.watch path.join(config.dev_path_template_part, '*.ejs'),     ['gen_template_part_live']
  gulp.watch path.join(config.dev_path_doc, '*.md'),                ['gen_doc_md_live']
  gulp.watch path.join(config.dev_path_doc, '*.html'),              ['gen_doc_html_live']
  $.livereload.listen
    port: 35729
  return

gulp.task 'gen_doc', gulpsync.sync [
  'gen_doc_md'
  'gen_doc_pdf'
  'gen_doc_html'
]

gulp.task 'default', gulpsync.sync [
  'clean'
  'copy_dir'
  'gen_coffee_compare'
  'gen_js'
  'gen_css'
  'gen_html'
  'gen_template_part'
  'gen_doc'
]

gulp.task 'template', gulpsync.sync [
  'gen_template_part'
]

gulp.task 'css', gulpsync.sync [
  'gen_css'
]
